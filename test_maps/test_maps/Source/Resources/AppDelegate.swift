//
//  AppDelegate.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private let coreDataManager = CoreDataManager.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        setupGoogleServices()
        setupRootViewController()

        return true
    }
    
    private func setupRootViewController() {
        let view: MapViewController = .instantiate(storyboard: .map)
        self.window?.rootViewController = view
        self.window?.makeKeyAndVisible()
    }
    
    private func setupGoogleServices() {
        GMSServices.provideAPIKey(Constants.GoogleMaps.apiKey)
        GMSPlacesClient.provideAPIKey(Constants.GoogleMaps.apiKey)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        coreDataManager.saveContext()
    }

}

