//
//  AlertPresenter.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit

protocol AlertPresenterProtocol: class {
    func showAlert(title: String, message: String, actions: [UIAlertAction])
}

extension AlertPresenterProtocol where Self: UIViewController {
    
    func showAlert(title: String = "error".localized,
                   message: String,
                   actions: [UIAlertAction] = []) {
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurVisualEffectView = UIVisualEffectView(effect: blurEffect)
        blurVisualEffectView.frame = view.bounds
        blurVisualEffectView.tag = 999
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if actions.isEmpty {
            alert.addAction(UIAlertAction(title: "close".localized, style: .default, handler: { _ in
                blurVisualEffectView.removeFromSuperview()
            }))
        } else {
            actions.forEach { action in
                alert.addAction(action)
            }
        }
        self.view.addSubview(blurVisualEffectView)
        self.present(alert, animated: true, completion: nil)
    }
    
}
