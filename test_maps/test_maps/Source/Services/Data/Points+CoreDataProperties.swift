//
//  Points+CoreDataProperties.swift
//  
//
//  Created by mac on 22.12.2020.
//
//

import Foundation
import CoreData


extension Points {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Points> {
        return NSFetchRequest<Points>(entityName: "Points")
    }

    @NSManaged public var lng: Double
    @NSManaged public var lat: Double

}
