//
//  DeviceInformationService.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit

protocol DeviceInformationServiceProtocol: class {
    var locale: String { get }
}

final class DeviceInformationService: DeviceInformationServiceProtocol {

    var locale: String {
        guard let locale = Locale.current.languageCode else { return "ua"}
        switch locale {
        case "uk":
            return "ua"
        default:
            return locale
        }
    }
    
}
