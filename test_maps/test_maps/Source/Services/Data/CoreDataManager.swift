//
//  CoreDataManager.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import CoreData
import UIKit
import CoreLocation

private struct CoreDataKeys {
    static let modelName = "test_maps"
    static let entityName = "Points"
    static let latKey = "lat"
    static let lngKey = "lng"
}

final class CoreDataManager {
    
    static let shared = CoreDataManager()
    private init() {}
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: CoreDataKeys.modelName)
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext() {
        let context = CoreDataManager.shared.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
    }
    
    func addPoint(_ point: CLLocationCoordinate2D) {
        let managedContext = CoreDataManager.shared.persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: CoreDataKeys.entityName, in: managedContext) else { return }
        let item = NSManagedObject(entity: entity, insertInto: managedContext)
        item.setValue(point.latitude, forKeyPath: CoreDataKeys.latKey)
        item.setValue(point.longitude, forKeyPath: CoreDataKeys.lngKey)
        do {
            try managedContext.save()
        } catch {
            debugPrint("Could not save. \(error)")
        }
    }
    
    func getSavedPoints() -> [CLLocationCoordinate2D] {
        var points: [CLLocationCoordinate2D] = []
        let managedContext = CoreDataManager.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataKeys.entityName)
        do {
            let item = try managedContext.fetch(fetchRequest)
            guard let savedPoints = item as? [Points] else { return [] }
            for item in savedPoints {
                points.append(item.toCoordinateItem)
            }
            return points
        } catch {
            debugPrint("Could not get. \(error)")
            return []
        }
    }
    
    func clearSavedPoints() {
        let managedContext = CoreDataManager.shared.persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: CoreDataKeys.entityName)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            debugPrint("\(error)")
        }
    }
    
}
