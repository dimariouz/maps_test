//
//  Points+CoreDataClass.swift
//  
//
//  Created by mac on 22.12.2020.
//
//

import Foundation
import CoreData

@objc(Points)
public class Points: NSManagedObject {

}

extension Points {
    
    var toCoordinateItem: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.lat, longitude: self.lng)
    }
    
}
