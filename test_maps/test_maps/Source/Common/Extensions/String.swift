//
//  String.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit

extension String {
    
    public var localized: String {
        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
    }
    
}

extension Bundle {
    
    public static var localizedBundle: Bundle {
        let code: DeviceInformationServiceProtocol = DeviceInformationService()
        guard let path = Bundle.main.path(forResource: code.locale, ofType: "lproj"),
            let bundle = Bundle(path: path) else {
                return Bundle.main
        }
        return bundle
    }
}
