//
//  UIView.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit

extension UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
