//
//  MapView.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit
import GoogleMaps
import CoreLocation

final class MapView: UIView {

    @IBOutlet private var contentView: UIView!
    @IBOutlet weak private var mapView: GMSMapView!
    
    private let networkService: NetworkServiceProtocol = NetworkService()
    private let zoom: Float = 15
    private let strokeWidth: CGFloat = 7.0
    private let pointsMaxCount = 10
    
    var warningMessageClosure: Closure<String>?
    var pointsArrayDidChange: Closure<[CLLocationCoordinate2D]>?
       
    private var pointsArray: [CLLocationCoordinate2D] = [] {
        didSet {
            showPath()
            pointsArrayDidChange?(pointsArray)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        Bundle.main.loadNibNamed(MapView.reuseIdentifier, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        updateCamera(point: Constants.GoogleMaps.startPoint)
        setupMapView()
    }
    
    private func setupMapView() {
        mapView.delegate = self
    }
    
    private func showPath() {
        addMarkers()
        loadRoutes(positions: pointsArray) { result in
            switch result {
            case .success(let route):
                self.configureMap(from: route)
            case .failure:
                self.warningMessageClosure?("drawing_path_error".localized)
            }
        }
    }
    
    private func loadRoutes(positions : [CLLocationCoordinate2D], completion: @escaping (Result<Route, Error>) -> Void ) {
        guard positions.count > 1,
              let startPoint = positions.first,
              let endPoint = positions.last else { return }
        var route = ""
        for point in positions {
            route += route.isEmpty ? "\(point.latitude),\(point.longitude)" : "|\(point.latitude),\(point.longitude)"
        }
        let path = "directions/json?origin=\(startPoint.latitude),\(startPoint.longitude)&destination=\(endPoint.latitude),\(endPoint.longitude)&waypoints=\(route)&key=\(Constants.GoogleMaps.apiKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        networkService.get(host: Constants.GoogleMaps.routesHost, path: path, completion: completion)
    }
    
    private func configureMap(from route: Route) {
        let allPath: GMSMutablePath = GMSMutablePath()
        route.routes
            .flatMap { $0.legs }
            .flatMap { $0.steps }
            .forEach {
                let path = GMSMutablePath(fromEncodedPath: $0.polyline.points)
                allPath.appendPath(path: path)
            }
        drawLine(with: allPath)
        updateCamera(point: pointsArray.last ?? Constants.GoogleMaps.startPoint)
    }
    
    private func drawLine(with path: GMSPath) {
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = strokeWidth
        polyline.strokeColor = .systemBlue
        polyline.map = mapView
    }
    
    private func updateCamera(point: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition(target: point, zoom: self.zoom, bearing: 0, viewingAngle: 0)
        mapView.camera = camera
        mapView.animate(toLocation: point)
    }
    
    private func addMarkers() {
        pointsArray.forEach {
            let marker = GMSMarker()
            marker.map = mapView
            marker.position = $0
        }
    }
    
    func popLastPoint() {
        guard !pointsArray.isEmpty else {
            warningMessageClosure?("no_points".localized)
            return
        }
        mapView.clear()
        _ = pointsArray.popLast()
    }
    
    func clearPath() {
        guard !pointsArray.isEmpty else {
            warningMessageClosure?("no_points".localized)
            return
        }
        mapView.clear()
        pointsArray.removeAll()
    }
    
    func configureView(with points: [CLLocationCoordinate2D]) {
        pointsArray = points
    }

}

extension MapView: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        guard pointsArray.count < pointsMaxCount else {
            warningMessageClosure?("max_points".localized)
            return
        }
        pointsArray.append(coordinate)
        addMarkers()
    }
    
}

extension GMSMutablePath {
    
    func appendPath(path : GMSPath?) {
        if let path = path {
            for i in 0..<path.count() {
                self.add(path.coordinate(at: i))
            }
        }
    }
}
