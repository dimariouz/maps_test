//
//  Constants.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit
import CoreLocation

typealias Closure<T> = (T) -> Void

struct Constants {
    
    enum Storyboards: String {
        case map = "Map"
    }
    
    struct GoogleMaps {
        static let routesHost = "https://maps.googleapis.com/maps/api/"
        static let apiKey = "AIzaSyCwgnavXNipuPIFFMktaNuk8TQj37viB8w"
        static let startPoint: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 48.466095, longitude: 35.049868)
    }

}
