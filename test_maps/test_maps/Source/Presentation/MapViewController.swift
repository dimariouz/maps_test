//
//  MapViewController.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit
import CoreLocation

final class MapViewController: UIViewController, AlertPresenterProtocol {

    @IBOutlet weak private var mapView: MapView!
    @IBOutlet weak private var cancelButton: UIButton!
    @IBOutlet weak private var clearButton: UIButton!
    
    private let model: MapViewModelProtocol = MapViewModel()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        setupMapView()
        setupButtons()
    }
    
    private func setupMapView() {
        mapView.configureView(with: model.pointsArray)
        mapView.warningMessageClosure = { [weak self] text in
            self?.showAlert(message: text)
        }
        mapView.pointsArrayDidChange = { [weak self] array in
            self?.model.pointsArray = array
            self?.configureButtonsAppearing()
        }
    }
    
    private func setupButtons() {
        cancelButton.setTitle("undo".localized, for: .normal)
        clearButton.setTitle("clear_all".localized, for: .normal)
        configureButtonsAppearing()
    }
    
    private func configureButtonsAppearing() {
        cancelButton.isHidden = model.pointsArray.isEmpty
        clearButton.isHidden = model.pointsArray.isEmpty
    }
        
    private func handleCancelingLastPoint() {
        mapView.popLastPoint()
        configureButtonsAppearing()
    }
    
    private func handleClearPath() {
        mapView.clearPath()
        configureButtonsAppearing()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        handleCancelingLastPoint()
    }
    
    @IBAction func clearButtonAction(_ sender: UIButton) {
        handleClearPath()
    }
    
}

