//
//  MapViewModel.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import Foundation
import CoreLocation

protocol MapViewModelProtocol: class {
    var pointsArray: [CLLocationCoordinate2D] { get set }
}

final class MapViewModel: MapViewModelProtocol {
        
    var pointsArray: [CLLocationCoordinate2D] = CoreDataManager.shared.getSavedPoints() {
        didSet {
            saveToCoreData(points: pointsArray)
        }
    }

    private func saveToCoreData(points: [CLLocationCoordinate2D]) {
        CoreDataManager.shared.clearSavedPoints()
        guard !points.isEmpty else { return }
        for point in pointsArray {
            CoreDataManager.shared.addPoint(point)
        }
    }
}
