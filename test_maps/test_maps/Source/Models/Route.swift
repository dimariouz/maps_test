//
//  Route.swift
//  test_maps
//
//  Created by mac on 22.12.2020.
//

import UIKit

struct Route: Codable {
    let routes: [Routes]
}

struct Routes: Codable {
    let polyline: Polyline
    let legs: [Leg]
    
    enum CodingKeys: String, CodingKey {
        case polyline = "overview_polyline"
        case legs = "legs"
    }
}

struct Polyline: Codable {
    let points: String
}

struct Leg: Codable {
    let steps: [LegStep]
}

struct LegStep: Codable {
    let polyline: Polyline
}
